; hello-world.z80
CHAN_OPEN   equ  5633
PRINT       equ  8252

            org  32512

            ld   a, 2                ; 3E 02
            call CHAN_OPEN           ; CD 01 16
            ld   de, text            ; 11 0E 7F
            ld   bc, textend-text    ; 01 0E 00
            jp   PRINT               ; C3 3C 20

text        defb 'Hello, World!'     ; 48 65 6C 6C 6F 2C 20 57
                                     ; 6F 72 6C 64 21
            defb 13                  ; 0D

textend     equ  $