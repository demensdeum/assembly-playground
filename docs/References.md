Z80:
    Spectrum:
        https://marguer.at/z80-assembly-code-for-the-zx-spectrum/ 
        https://atornblad.se/hello-world-in-zx-spectrum-machine-code/
        http://www.pauldjohnson.me/2020/02/

    Sega Game Gear & Master System:
        https://www.chibiakumas.com/z80/helloworld.php#LessonH8
        https://www.youtube.com/watch?v=T3XwLb9Ktv8&list=PLp_QNRIYljFpKtZHSAFZ9EUlpe-W0_BHg&index=1
        https://www.smspower.org/maxim/HowToProgram/Index

X86-64:
    http://www.egr.unlv.edu/~ed/assembly64.pdf
    https://jameshfisher.com/2018/03/10/linux-assembly-hello-world/
    https://www.devdungeon.com/content/how-mix-c-and-assembly
    https://aaronbloomfield.github.io/pdr/book/x86-64bit-ccc-chapter.pdf
